# Basic Sass grid UI Boilerplate

## Table of contents
* [General info](#general-info)
* [Setup](#Setup)
* [How to run](#How-to-run)
* [Gulpfile](#Gulpfile)
* [yml sass lint](#yml-sass-lint)
* [Basic folder structure](#Basic-folder-structure)
* [Browse Support](#Browse-Support)

## General info
This project is simple sass grid front ui boilerplate and a NPM based boilerplate to easily setup a frontend development environment. 
* Referencias
* url to link: https://www.youtube.com/watch?v=kKcFN6SMwlk
* url to link: https://youtu.be/QgMQeLymAdU
* url to link: https://codepen.io/anon/pen/YgmzPv
* url to link: https://github.com/sasstools/sass-lint
* url to link: https://www.youtube.com/watch?v=BaIgpEzl0CM
	
## Setup
Install gulp gulp-sass browser-sync:
* npm init --yes
* create sass folder
* create css folder
* npm install gulp-cli -g
* npm install --save-dev gulp gulp-sass browser-sync
* Create Gulpfile.js

Install sass-lint:
* npm install sass-lint --save-dev || npm install -g sass-lint
* Create .scss-lint.yml
* modifid package.json includ rules into script: "lint:sass": "sass-lint scss/**/*.scss -c -v" 

```
$ cd ../folder
$ npm init --yes
$ npm install gulp-cli -g
$ npm install --save-dev gulp gulp-sass browser-sync
npm install sass-lint --save-dev || npm install -g sass-lint
touch .scss-lint.yml
```
## How to run 
* gulp watch
* npm run lint:sass


## Gulpfile
```
var gulp = require('gulp');  
var sass = require('gulp-sass');  
var browserSync = require('browser-sync').create();

//compile scss into css
function style(){
    // where is my scss file
    return gulp.src('./scss/**/*.scss')
    // pass that file through sass compiler
    .pipe(sass())
    //compress
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    //where do i save the compile css
    .pipe(gulp.dest('./css'))
    //stream changes to all browser
    .pipe(browserSync.stream());

}

function watch(){
    browserSync.init({
        server:{
            baseDir: './'
        }
    })
    gulp.watch('./scss/**/*.scss', style);
    gulp.watch('./*.html').on('change',browserSync.reload);
    gulp.watch('./js/**/*.js').on('change',browserSync.reload);

}

exports.style = style
exports.watch = watch

```


## yml sass lint
```
# http://sass-guidelin.es/es/
# For SCSS-Lint v0.32.0

linters:

  BangFormat:
    enabled: true
    space_before_bang: true
    space_after_bang: false

  BorderZero:
    enabled: true

  ColorVariable:
    enabled: true

  Comment:
    enabled: false

  DebugStatement:
    enabled: true

  DeclarationOrder:
    enabled: true

  DuplicateProperty:
    enabled: false

  ElsePlacement:
    enabled: true
    style: same_line

  EmptyLineBetweenBlocks:
    enabled: true
    ignore_single_line_blocks: true

  EmptyRule:
    enabled: true

  FinalNewline:
    enabled: true
    present: true

  HexLength:
    enabled: true
    style: long

  HexNotation:
    enabled: true
    style: lowercase

  HexValidation:
    enabled: true

  IdSelector:
    enabled: true

  ImportantRule:
    enabled: true

  ImportPath:
    enabled: true
    leading_underscore: false
    filename_extension: false

  Indentation:
    enabled: true
    character: space
    width: 2

  LeadingZero:
    enabled: true
    style: exclude_zero

  MergeableSelector:
    enabled: true

  NameFormat:
    enabled: true
    convention: hyphenated_lowercase
    allow_leading_underscore: true

  NestingDepth:
    enabled: true
    max_depth: 4

  PlaceholderInExtend:
    enabled: true

  PropertySortOrder:
    enabled: true

  PropertySpelling:
    enabled: true
    extra_properties: []

  SelectorDepth:
    enabled: true
    max_depth: 3

  SelectorFormat:
    enabled: false
    class_convention: '^(?:[a-zA-Z0-9]*)\-[A-Z]{1}[a-z][a-zA-Z0-9]*(?:\-[a-z][a-zA-Z0-9]*)?(?:\-\-[a-z][a-zA-Z0-9]*)?$'

  Shorthand:
    enabled: true

  SingleLinePerProperty:
    enabled: true
    allow_single_line_rule_sets: false

  SingleLinePerSelector:
    enabled: true

  SpaceAfterComma:
    enabled: true

  SpaceAfterPropertyColon:
    enabled: true
    style: at_least_one_space

  SpaceAfterPropertyName:
    enabled: true

  SpaceBeforeBrace:
    enabled: true
    style: space
    allow_single_line_padding: true

  SpaceBetweenParens:
    enabled: true
    spaces: 0

  StringQuotes:
    enabled: true
    style: single_quotes

  TrailingSemicolon:
    enabled: true

  TrailingZero:
    enabled: true

  UnnecessaryMantissa:
    enabled: true

  UnnecessaryParentReference:
    enabled: true

  UrlFormat:
    enabled: false

  UrlQuotes:
    enabled: false

  VendorPrefixes:
    enabled: true

  ZeroUnit:
    enabled: true

```


## Basic folder structure

```
Project-Name/
    ├── images/
    ├── css/
    │   └── main.css
    └── scss/
        ├── _variables.scss
        ├── _mixins.scss
        └── main.scss
    └── js/
        └── script.js   
Index.html
README.md
Gulpfile.js
.scss-lint-yml
.env
```



## Browse Support
Caveats and Known Issues
* IE 10 has Flexbox support but for a draft version of the current spec: (display:flexbox).
* Safari 6 and earlier support the original Flexbox syntax, which is now obsolete: (display:box).
* Firefox 27 and earlier do not support multi-line flexboxes. See this bug for more details.
* For a full browser support comparison, check out caniuse.com/flexbox


![Algorithm schema](./images/browseSupport.png)
