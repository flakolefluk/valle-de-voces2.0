// Recoge el listado JSON e imprime funcción drawMiniCard(dato)
function fetchF() {
  var url = 'data.json';
  fetch(url)
    .then((response) => response.json())
    .then((el) => {
      drawMiniCard(el); //Usar cuando la representación de thubms es sin random
      drawOpenModal();
    });
}

function drawMiniCard(dato) {
  let grid = '';
  for (let i in dato) {
    grid += "<div data-key='" +
      i +
      "' class='card' onclick='drawModal(" +
      dato[i].id +
      ', "' +
      dato[i].location +
      '" , "' +
      dato[i].commune +
      '")\'>' +
      "<div class='card__image'>" +
      "<img src='https://www.valledevoces.cl/images/thumbnails/" +
      dato[i].id +
      ".jpg'  />" +
      '</div>' +
      '</div>';
  }
  document.querySelector('.main__content').innerHTML += grid;
}

function drawModal(id, location, commune) {
  var newModal = document.createElement('div');
  newModal.setAttribute('id', 'modal');
  newModal.className = 'modal modal--show ';
  newModal.innerHTML =
    "<span id='modal__button-close' class='modal__close' onclick='closeModal(this.id, close)'> </span>" +
    "<span id='wavePlay-icon' class='wave-play'></span>" +
    "<div class='modal__content'>" +
    "<div id='waveform' class='modal__waveform'>" +
    "<div class='modal__info'>" +
    "<span class='modal__location'>" +
    location +
    '</span> -' +
    "<span class='modal__commune'> " +
    commune +
    '</span>' +
    '</div>' +
    '</div>' +
    "<img id ='imgModal' src='https://www.valledevoces.cl/images/modalimage/" +
    id +
    ".jpg' alt='nombre' />" +
    '</div>';
  var newBkgModal = document.createElement('div');
  newBkgModal.className = 'bkg-modal bkg-modal--show';
  document.getElementById('content').appendChild(newModal);
  document.getElementById('content').appendChild(newBkgModal);
  newModal.classList.remove('modal--hide');
  newModal.classList.add('modal--show');
  newBkgModal.classList.remove('bkgModal--hide');
  newBkgModal.classList.add('bkgModal--show');
  waveAudio(id);
}

function closeModal(clicked_id) {
  var x = document.getElementById(clicked_id).parentNode;
  var y = x.nextSibling;
  x.classList.remove('modal--show');
  x.classList.add('modal--hide');
  y.classList.remove('bkgModal--show');
  y.classList.add('bkgModal--hide');
  x.parentNode.removeChild(x);
  y.remove();
}

function drawOpenModal() {
  var newModal = document.createElement('div');
  newModal.setAttribute('id', 'open-modal');
  newModal.className = 'modal modal--show open-modal';
  newModal.innerHTML =
    "<span id='modal__button-close' class='modal__close' onclick='closeModal(this.id, close)'></span>" +
    "<div class='modal__content'>" +
    "<span class='modal__content--title-logo'>Valle de Voces</span>" +
    "<span class='modal__content--slogan'>Memoria Hablada del Valle del Itata </span>" +
    '<p>El siguiente sitio opera como banco sonoro y visual del acervo cultural tradicional de los habitantes del territorio rural del Valle del Itata (Región del Ñuble) y tiene como objetivo contribuir a la puesta en valor de la identidad rural de este territorio.</p>' +
    '<p>El contenido principal de este sitio son cápsulas de audio por lo que se recomienda el uso de audífonos.</p>' +
    '</div>' +
    "<canvas id='c'></canvas>" +
    '</div>';
  var newBkgModal = document.createElement('div');
  newBkgModal.className = 'bkg-modal-oppening bkg-modal-oppening--show';
  document.getElementById('content').appendChild(newModal);
  document.getElementById('content').appendChild(newBkgModal);
  newModal.classList.remove('modal--hide');
  newModal.classList.add('modal--show');
  newBkgModal.classList.remove('bkg-modal-oppening--hide');
  newBkgModal.classList.add('bkg-modal-oppening--show');
}

document.getElementById('trigger').addEventListener('click', function () {
  document.querySelector('.target').classList.toggle('max');
});

function waveAudio(id) {
  var wavesurfer = WaveSurfer.create({
    container: '#waveform',
    waveColor: '#cccccc',
    progressColor: '#e05b4d',
    barHeight: 2,
    height: 85,
  });
  wavesurfer.load('../media/' + id + '.mp3');

  wavesurfer.on('ready', function () {
    var playAudio = document.getElementById('wavePlay-icon');
    playAudio.addEventListener('click', function () {
      // e.stopPropagation();
      // wavesurfer.stop();
      wavesurfer.play();
      console.log('../media/' + id + '.mp3');
      playAudio.remove();
    });
  });

  var stopAudio = document.getElementById('modal__button-close');
  stopAudio.addEventListener('click', function (e) {
    e.stopPropagation();
    wavesurfer.stop();
    wavesurfer.destroy();
  });
}

fetchF();
